package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button action;
    EditText login;
    EditText email;
    EditText number;
    EditText password;
    EditText rePassword;
    TextView loginError;
    TextView passwordError;
    TextView numberError;
    TextView emailError;
    TextView rePasswordError;
    final String mail="@gmail.com";
    final String phone="+380";
    final int numberOfSymbolsPhoneNumber=13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        action = findViewById(R.id.action);
        login = findViewById(R.id.login);
        email = findViewById(R.id.email);
        number = findViewById(R.id.number_of_telephone);
        password = findViewById(R.id.password);
        rePassword = findViewById(R.id.re_password);
        loginError = findViewById(R.id.login_error);
        passwordError=findViewById(R.id.password_error);
        numberError=findViewById(R.id.phone_error);
        emailError=findViewById(R.id.email_error);
        rePasswordError=findViewById(R.id.re_password_error);

        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((!checkAndShowErrors())&&(checkPasswordsOnSimalarity())&&(checkEmail())&&(checkPhone())) {
                    Toast.makeText(MainActivity.this, "Верификация прошла успешно!", Toast.LENGTH_SHORT).show();
                }
                else if (!checkPasswordsOnSimalarity()){
                    Toast.makeText(MainActivity.this, "Пароли не совпадают! Попробуйте ещё раз", Toast.LENGTH_SHORT).show();
                }

                else if (!checkEmail()){
                    Toast.makeText(MainActivity.this, "Почта не отвечатет требованиям! Попробуйте ещё раз", Toast.LENGTH_SHORT).show();
                }

                else if (!checkPhone()){
                    Toast.makeText(MainActivity.this, "Номер телефона не отвечатет требованиям! Попробуйте ещё раз", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean checkPhone(){
        boolean hasError=false;
        if((number.getText().toString().contains(phone))&&(number.getText().length()==numberOfSymbolsPhoneNumber)){
            hasError=true;
        }
        return hasError;
    }


    private boolean checkEmail(){
        boolean hasError=false;
        if(email.getText().toString().contains(mail)){
            hasError=true;
        }
        return hasError;
    }



    private boolean checkPasswordsOnSimalarity(){
        boolean hasError=false;
        if(password.getText().toString().equals(rePassword.getText().toString())){
            hasError=true;
        }
        return hasError;
    }

    private boolean checkAndShowErrors() {
        boolean hasError = false;
        if (login.getText().toString().isEmpty()) {
            hasError = true;
            loginError.setVisibility(View.VISIBLE);
        } else {
            loginError.setVisibility(View.INVISIBLE);
        }

        if (email.getText().toString().isEmpty()) {
            hasError = true;
            emailError.setVisibility(View.VISIBLE);
        } else {
            emailError.setVisibility(View.INVISIBLE);
        }

        if (number.getText().toString().isEmpty()) {
            hasError = true;
            numberError.setVisibility(View.VISIBLE);
        } else {
            numberError.setVisibility(View.INVISIBLE);
        }

        if (password.getText().toString().isEmpty()) {
            hasError = true;
            passwordError.setVisibility(View.VISIBLE);
        } else {
            passwordError.setVisibility(View.INVISIBLE);
        }

        if (rePassword.getText().toString().isEmpty()) {
            hasError = true;
            rePasswordError.setVisibility(View.VISIBLE);
        } else {
            rePasswordError.setVisibility(View.INVISIBLE);
        }


        return hasError;
    }
}
